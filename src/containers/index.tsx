import * as React from "react";
import { Hambuger } from "../components/hamburger";
import { NavBar } from "../components/nabar";
import { Card } from "../components/card/";
import { ButtonWithText, ButtonWithIcon, ButtonGroup } from "../components/buttons/";

interface AppProps { }

interface AppState { }

export class App extends React.Component<AppProps, AppState> {
    protected leftMenu: Hambuger;
    constructor(props: AppProps) {
        super(props);
    }
    render() {
        const { children } = this.props;
        return (
            <div className="app">
                <div className="app-container">
                    <div className="app-body">
                        <NavBar>
                            <NavBar.NavItem>
                                <i onClick={e => {
                                    this.leftMenu.toggle();
                                    e.stopPropagation();
                                }} className="material-icons">menu</i>
                                <div className="app-header-title">Trusted.login</div>
                            </NavBar.NavItem>
                            <NavBar.NavItem>
                                <div className="input-left-up">
                                    <div className="app-header-title enter">Войти</div>
                                    <i className="material-icons blocked">lock_outline</i>
                                </div>
                            </NavBar.NavItem>
                        </NavBar>
                        <div className="app-body-container">
                            <div className="app-body-content">
                                <div className="container">
                                    <Card title="Hello world">
                                        Card 1 Content
                                    </Card>

                                    <Card title="Hello world">
                                        Card 2 Content
                                    </Card>

                                    <ButtonGroup>
                                        <ButtonWithText
                                            text="Alert"
                                            onClick={() => { alert("Hello"); }} />

                                        <ButtonWithText
                                            text="Disabled"
                                            disabled
                                            onClick={() => { alert("Hello"); }} />

                                        <ButtonWithIcon icon="+" />
                                    </ButtonGroup>
                                </div>
                            </div>
                        </div>
                    </div>
                    <Hambuger
                        ref={(e) => this.leftMenu = e}>
                        Меню
                    </Hambuger>
                </div>
            </div>
        );
    }
}
