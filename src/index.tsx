import * as React from 'react';
import { render } from 'react-dom';
import { Router, Route, IndexRedirect, hashHistory } from 'react-router';

// Containers
import { App } from './containers/index';

render(
    <App />,
    document.getElementById('app')
);