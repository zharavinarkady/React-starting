import * as React from "react";

interface CardTitleProps {
    title?: string;
}

class Title extends React.PureComponent<CardTitleProps, {}> {
    render() {
        return (
            <div className="card-wrapper-title">
                {this.props.title}
            </div>
        )
    }
}

interface CardProps {
    title?: string;
}

export class Card extends React.Component<CardProps, {}> {

    render() {
        const title = this.props.title ? <Title title={this.props.title} /> : null;
        return (
            <div className="card-wrapper">
                {title}

                <div className="card-wrapper-content">
                    {this.props.children}
                </div>
            </div>
        )
    }
}