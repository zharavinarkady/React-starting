import * as React from 'react';

class Item extends React.PureComponent<{}, {}> {
    render() {
        return (
            <div className="nav-item--wrapper">
                {this.props.children}
            </div>
        )
    }
}


export class NavBar extends React.Component<{}, {}>{
    public static NavItem = Item;

    render() {
        return (
            <div className="bar-wrapper">
                {this.props.children}
            </div>
        );

    }
}
