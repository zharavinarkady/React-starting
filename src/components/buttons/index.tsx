import * as React from "react";

interface ButtonProps {
    onClick?: Function;
    disabled?: boolean;
}

interface BaseButtonProps extends ButtonProps {
    type: "text" | "round";
}

export class Button extends React.Component<BaseButtonProps, {}> {
    render() {
        return (
            <div
                className={`btn ${this.props.disabled ? "disabled" : ""} ${this.props.type}`}
                onClick={(e) => { !this.props.disabled && this.props.onClick(e); }}>

                {this.props.children}
            </div>
        )
    }
}

interface ButtonWithTextProps extends ButtonProps {
    text: string;
}

export class ButtonWithText extends React.Component<ButtonWithTextProps, {}> {
    render() {
        return (
            <Button
                type="text"
                disabled={this.props.disabled}
                onClick={this.props.onClick}>
                <div className="btn-text-content">
                    {this.props.text}
                </div>
            </Button>
        )
    }
}

interface ButtonWithIconProps extends ButtonProps {
    icon: string;
}

export class ButtonWithIcon extends React.Component<ButtonWithIconProps, {}> {
    render() {
        return (
            <Button
                type="round"
                disabled={this.props.disabled}
                onClick={this.props.onClick}>
                <div className="btn-text-content">
                    {this.props.icon}
                </div>
            </Button>
        )
    }
}

export class ButtonGroup extends React.PureComponent<{}, {}> {
    render() {
        return (
            <div className="btn-group">
                {this.props.children}
            </div>
        )
    }
}