import * as React from "react";

interface IHamburgerProps { }
interface IHamburgerState {
    hidden?: boolean;
}

export class Hambuger extends React.PureComponent<IHamburgerProps, IHamburgerState> {

    constructor(props: IHamburgerProps) {
        super(props);

        this.state = {
            hidden: true
        }
    }

    public toggle() {
        this.setState({
            hidden: !this.state.hidden
        });
    }

    render() {
        return (
            <div
                onClick={() => this.toggle()}
                className={`bg ${!this.state.hidden ? "show" : ""}`}>
                <div className={`left-menu ${!this.state.hidden ? "show" : ""}`}>
                    <div>
                        Менюшка
                    </div>
                </div>
            </div>
        );
    }
}