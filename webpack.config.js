const ExtractTextPlugin = require("extract-text-webpack-plugin");
const webpack = require("webpack");
const autoprefixer = require("autoprefixer");

const ENV_PROD = process.argv.includes('--env=prod');
console.log(`Is ${ENV_PROD ? "" : "NOT "}production`);

function configure(env) {
    return [
        {
            entry: {
                landing: "./src/index.tsx",
                styles: "./src/index.scss"
            },
            output: {
                filename: "[name].js",
                path: "./build"
            },
            resolve: {
                extensions: [".js", ".jsx", ".ts", ".tsx"]
            },

            module: {
                rules: [
                    {
                        test: /\.tsx?$/,
                        use: "ts-loader"
                    },
                    {
                        test: /\.scss$/,
                        use: ExtractTextPlugin.extract({
                            fallbackLoader: 'style-loader',
                            use: ['css-loader', 'sass-loader', {loader: 'postcss-loader'}],
                            publicPath: './test'
                        })
                    }

                ]
            },
            plugins: [
                new webpack.DefinePlugin(
                    {
                        "process.env": {
                            NODE_ENV: JSON.stringify(env === ENV_PROD ? "production" : "development")
                        }
                    }),
                new ExtractTextPlugin({
                    filename: "landing.css",
                    allChunks: true
                }),
                new webpack.LoaderOptionsPlugin({
                    debug: false,
                    options: {
                        minimize: env === ENV_PROD,
                        resolve: {},
                        postcss: [
                            autoprefixer()
                        ]
                    }
                }),
                ENV_PROD ? new webpack.optimize.UglifyJsPlugin() : function () {}
            ],
            externals: ENV_PROD ? {
                "react": "React",
                "react-dom": "ReactDOM",
                "react-router": "ReactRouter"
            } : {}
        }
    ];
}

module.exports = configure;
